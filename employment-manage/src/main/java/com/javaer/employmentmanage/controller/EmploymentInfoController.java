package com.javaer.employmentmanage.controller;

import com.javaer.employmentmanage.common.CommonResult;
import com.javaer.employmentmanage.mapper.entity.EmploymentInfo;
import com.javaer.employmentmanage.service.EmploymentInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.UUID;

@Controller
public class EmploymentInfoController {
    @Autowired
    EmploymentInfoService employmentInfoService;

    @RequestMapping({"/employmentmanage/index", "/employmentmanage/employmentinfo"})
    public String index(){
        return "system/employmentinfo/employmentinfo";
    }

    @ResponseBody
    @RequestMapping("/employmentmanage/getallinfo")
    public CommonResult<List<EmploymentInfo>> getAllInfo(EmploymentInfo employmentInfo, @RequestParam("limit") int pageSize, @RequestParam("page") int pageNum){
        List<EmploymentInfo> infoList = employmentInfoService.getAllEmploymentInfo(employmentInfo, pageNum, pageSize);
        CommonResult<List<EmploymentInfo>> rtInfoResult = CommonResult.generateSuccessResult(infoList.size(), infoList);

        return rtInfoResult;
    }

    @ResponseBody
    @RequestMapping("/employmentmanage/getinfo")
    public CommonResult<List<EmploymentInfo>> getinfo(EmploymentInfo info, @RequestParam("limit") int pageSize, @RequestParam("page") int pageNum){
        List<EmploymentInfo> infoList = employmentInfoService.getEmploymentInfo(info, pageNum, pageSize);
        CommonResult<List<EmploymentInfo>> rtInfoResult = CommonResult.generateSuccessResult(infoList.size(), infoList);

        return rtInfoResult;
    }

    @ResponseBody
    @RequestMapping("/employmentmanage/addinfo")
    public CommonResult<Integer> addInfo(EmploymentInfo info){
        info.setInformationId(UUID.randomUUID().toString());
        employmentInfoService.addEmploymentInfo(info);
        return CommonResult.generateSuccessResult(1, 1);
    }

    @ResponseBody
    @RequestMapping("/employmentmanage/updateinfo")
    public CommonResult<Integer> updateInfo(EmploymentInfo info){
        employmentInfoService.updateEmploymentInfo(info);
        return CommonResult.generateSuccessResult(1, 1);
    }

    @ResponseBody
    @RequestMapping("/employmentmanage/delinfo/{infoId}")
    public CommonResult<Integer> delInfo(@PathVariable("infoId") String infoId){
        employmentInfoService.deleteEmploymentInfo(infoId);
        return CommonResult.generateSuccessResult(1, 1);
    }
}
