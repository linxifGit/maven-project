package com.javaer.employmentmanage.controller;

import com.javaer.employmentmanage.common.CommonResult;
import com.javaer.employmentmanage.mapper.entity.User;
import com.javaer.employmentmanage.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.UUID;

@Controller
public class UserController {
    @Autowired
    UserService userService;

    @RequestMapping("/employmentmanage/usermanage")
    public String index(){
        return "system/usermanage/usermanage";
    }

    @ResponseBody
    @RequestMapping("/employmentmanage/getallusers")
    public CommonResult<List<User>> getAllUsers(User user, @RequestParam("limit") int pageSize, @RequestParam("page") int pageNum){
        List<User> result = userService.getAllUsers(user, pageNum, pageSize);
        return CommonResult.generateSuccessResult(result.size(), result);
    }

    @ResponseBody
    @RequestMapping("/employmentmanage/getuserbyaccount/{userAccount}")
    public CommonResult<User> getUserByAccount(@PathVariable("userAccount") String userAccount){
        return CommonResult.generateSuccessResult(1, userService.getUserByAccount(userAccount));
    }

    @ResponseBody
    @RequestMapping("/employmentmanage/adduser")
    public CommonResult<Integer> addUser(User user){
        user.setUserId(UUID.randomUUID().toString());
        userService.addUser(user);
        return CommonResult.generateSuccessResult(1, 1);
    }

    @ResponseBody
    @RequestMapping("/employmentmanage/updateuser")
    public CommonResult<Integer> updateUser(User user){
        userService.updateUser(user);
        return CommonResult.generateSuccessResult(1, 1);
    }

    @ResponseBody
    @RequestMapping("/employmentmanage/deluser/{userId}")
    public CommonResult<Integer> delInfo(@PathVariable("userId") String userId){
        userService.deleteUser(userId);
        return CommonResult.generateSuccessResult(1, 1);
    }
}
