package com.javaer.employmentmanage.controller;

import com.javaer.employmentmanage.common.CommonResult;
import com.javaer.employmentmanage.mapper.entity.User;
import com.javaer.employmentmanage.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class HomeController {
    @Autowired
    UserService userService;

    @RequestMapping({"/","/employmentmanage"})
    public String index(){
        return "system/login";
    }

    @ResponseBody
    @RequestMapping("/employmentmanage/login")
    public CommonResult<User> login(User user){
        User loginUser = userService.getUserByAccount(user.getUserAccount());
        if(loginUser == null || !loginUser.getUserPwd().equals(user.getUserPwd())){
           return  CommonResult.generateFailureResult("帐号或密码不正确", 1, null);
        }else{
            return CommonResult.generateSuccessResult(1, loginUser);
        }
    }
}
