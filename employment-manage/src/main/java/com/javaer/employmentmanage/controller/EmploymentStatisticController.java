package com.javaer.employmentmanage.controller;

import com.javaer.employmentmanage.common.CommonResult;
import com.javaer.employmentmanage.service.EmploymentInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

@Controller
public class EmploymentStatisticController {
    @Autowired
    EmploymentInfoService employmentInfoService;

    @RequestMapping("/employmentmanage/statistic")
    public String index(){
        return "system/employmentstatistic/employmentstatistic";
    }

    @ResponseBody
    @RequestMapping("/employmentmanage/statistic/{fieldName}")
    public CommonResult<List<Map<String, String>>> getStatisticData(@PathVariable("fieldName") String fieldName,
                                                              @RequestParam("limit") int pageSize, @RequestParam("page") int pageNum){
        List<Map<String, String>> statisticResult = employmentInfoService.getStudentCount(fieldName, pageNum, pageSize);
        return CommonResult.generateSuccessResult(statisticResult.size(), statisticResult);
    }
}
