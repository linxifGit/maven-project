package com.javaer.employmentmanage;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.javaer.employmentmanage.mapper")
public class EmploymentManageApplication {

    public static void main(String[] args) {
        SpringApplication.run(EmploymentManageApplication.class, args);
    }

}
